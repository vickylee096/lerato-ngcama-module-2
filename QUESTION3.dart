class Object_class {
  List<String> apps = [
    "FNB 2012",
    "DSTV 2013",
    "Supersport 2014",
    "m4Jam 2015",
    "iKhoKha 2016",
    "shyft 2017",
    "be Smarta 2018",
    "vula 2019",
    "checkers 60 sixty 2020",
    "TakeAlot 2022"
  ];
  List<String> categories = [
    "Entertainment",
    "Educational",
    "utilities",
    "Music",
    "Lifestyle",
    "Entertainment",
    "Music",
    "Educational",
    "LifeStyle",
    "Educational"
  ];
  List<String> Developers = [
    "Jane Khumalo",
    "DOrah Mkhizi",
    "Lerato Moloi",
    "Zac Smith",
    "Mark baloyi",
    "Martha Sibiya",
    "Carolina sibanda",
    "Sipho Mbatha",
    "Khulekani Zwane",
    "Godfrey Motshoane"
  ];
  List<int> years = [
    2012,
    2013,
    2014,
    2015,
    2016,
    2017,
    2018,
    2019,
    2020,
    2021
  ];
  // Class Function
  showInfo() {
    print(apps);
    print(categories);
    print(Developers);
    print(years);
  }
}

void main() {
  // Creating Object called std
  var obj = new Object_class();
  obj.showInfo();
}
